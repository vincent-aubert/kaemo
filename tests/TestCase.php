<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        // Create sqlite database for tests
        Config::set('database', [
            'default' => 'sqlite',
            'connections' => [
                'sqlite' => [
                    'driver' => 'sqlite',
                    'database' => ':memory:',
                    'prefix' => ''
                ]
            ]
        ]);

        DB::connection()->setFetchMode(PDO::FETCH_CLASS);

        return $app;
    }

    /**
     * Create and seed database
     */
    private function prepareForTests()
    {
        $this->artisan('migrate');
        $this->seed('VideosTableSeeder');
    }

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->prepareForTests();
    }
}
