<?php

class VideoTest extends TestCase
{

    /**
     * Test index in VideoController
     *
     * @return void
     */
    public function testIndexWithoutParameters()
    {

        $response = $this->action('GET', 'VideoController@index')->getContent();
        $response = json_decode($response);

        $this->assertResponseOk();
        $this->assertCount(10, $response->videos);
        $this->assertEquals(10, $response->count);

    }

    /**
     * Test from parameter
     */
    public function testIndexWithFromParameter()
    {
        $response = $this->action('GET', 'VideoController@index', ['from' => '20160521'])->getContent();
        $response = json_decode($response);

        $this->assertResponseOk();
        $this->assertCount(4, $response->videos);
        $this->assertEquals(4, $response->count);
    }

    /**
     * Test to parameter
     */
    public function testIndexWithToParameter()
    {
        $response = $this->action('GET', 'VideoController@index', ['to' => '20160521'])->getContent();
        $response = json_decode($response);

        $this->assertResponseOk();
        $this->assertCount(7, $response->videos);
        $this->assertEquals(7, $response->count);
    }

    /**
     * Test with date range
     */
    public function testIndexWithFromAndParameter()
    {
        $response = $this->action('GET', 'VideoController@index', ['from' => '20160518', 'to' => '20160521'])->getContent();
        $response = json_decode($response);

        $this->assertResponseOk();
        $this->assertCount(4, $response->videos);
        $this->assertEquals(4, $response->count);
    }

    /**
     * Test search by director
     */
    public function testIndexWithDirectorParameter()
    {
        $response = $this->action('GET', 'VideoController@index', ['director' => 'Desplechin'])->getContent();
        $response = json_decode($response);

        $this->assertResponseOk();
        $this->assertCount(1, $response->videos);
        $this->assertEquals(1, $response->count);
    }

    /**
     * Test show in controller
     */
    public function testShow()
    {
        $response = $this->action('GET', 'VideoController@show', ['id' => '3'])->getContent();
        $response = json_decode($response);

        $this->assertResponseOk();
        $this->assertEquals("Krisha", $response->title);
    }

    /**
     * Test 404
     */
    public function testShowWithUnknownId()
    {
        $this->action('GET', 'VideoController@show', ['id' => '13'])->getContent();
        $this->assertResponseStatus(404);
    }

}
