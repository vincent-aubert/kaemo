<?php

use Illuminate\Database\Seeder;
use App\Video;
use Carbon\Carbon;

class VideosTableSeeder extends Seeder
{

    private $videos = [
        [
            "title" => "Manchester by the Sea",
            "director" => "Kenneth Lonergan"
        ],
        [
            "title" => "Little Men",
            "director" => "Ira Sachs"
        ],
        [
            "title" => "Krisha",
            "director" => "Trey Edward Shults"
        ],
        [
            "title" => "Cemetery of Splendor",
            "director" => "Apichatpong Weerasethakul"
        ],
        [
            "title" => "My Golden Days",
            "director" => "Arnaud Desplechin"
        ],
        [
            "title" => "Love & Friendship",
            "director" => "Whit Stillman"
        ],
        [
            "title" => "April and the Extraordinary World",
            "director" => "Christian Desmares and Franck Ekinci"
        ],
        [
            "title" => "Love & Friendship",
            "director" => "Whit Stillman"
        ],
        [
            "title" => "Fireworks Wednesday",
            "director" => "Asghar Farhadi"
        ],
        [
            "title" => "Everybody Wants Some!!",
            "director" => "Richard Linklater"
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $date = Carbon::createFromDate(2016, 5, 25)->hour(12)->minute(0);

        foreach ($this->videos as $video) {
            $model = new Video();
            $model->title = $video['title'];
            $model->director = $video['director'];
            $model->date = $date->subDay();
            $model->save();
        }

    }
}
