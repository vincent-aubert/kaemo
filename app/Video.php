<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Video
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $director
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Video whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Video whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Video whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Video whereDirector($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Video whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Video whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Video whereDeletedAt($value)
 */
class Video extends Model
{

    use SoftDeletes;

    protected $table = 'videos';

    protected $fillable = ['title', 'date', 'director'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

}
