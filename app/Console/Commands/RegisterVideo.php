<?php

namespace App\Console\Commands;

use App\Video;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RegisterVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register a new video';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Ask for user inputs
        $title = $this->ask("Please, give me the title of the video");
        $date = $this->askAndCheckDateFormat();
        $director = $this->ask("Last question, who directed the video ?");

        // Create new video entry
        $video = new Video();
        $video->title = $title;
        $video->date = $date;
        $video->director = $director;
        $video->save();

        $this->comment('Thank you ! Your video is registered !');
    }

    /**
     * Ask date and check if given date is valid
     *
     * @return string|static
     */
    private function askAndCheckDateFormat()
    {
        $date = $this->ask("What is the video registration date ?", Carbon::now());

        // Check date format
        try {
            $date = Carbon::parse($date);
            return $date;
        } catch (\Exception  $e) {
            $this->error("Oops ! $date is not a valid date format");
            return $this->askAndCheckDateFormat();
        }
    }
}
