<?php

namespace App\Http\Controllers;

use App\Video;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class VideoController extends Controller
{

    private $request;

    /**
     * VideoController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get video list
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {

        // Get filters
        $from = $this->request->has('from') ? Carbon::createFromFormat('Ymd', $this->request->get('from')) : null;
        $to = $this->request->has('to') ? Carbon::createFromFormat('Ymd', $this->request->get('to')) : null;
        $director = $this->request->get('director');

        // Get videos
        $videos = \DB::table('videos');

        // Apply filters
        if ($director) {
            $videos->where('director', 'like', "%$director%");
        }

        if ($from) {
            $videos->where('date', '>=', $from->hour(0));
        }

        if ($to) {
            $videos->where('date', '<=', $to->hour(23)->minute(59)->second(59));
        }

        // Create response
        return response([
            'videos' => $videos->get(),
            'count' => $videos->count()
        ]);
    }

    /**
     * Get requested video
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function show($id)
    {
        $video = Video::findOrFail($id);
        return response($video);
    }
}
