# Test Kaemo

## Installation

Commencer par clôner le repo

``` git clone https://gitlab.com/vincent-aubert/kaemo.git ```

``` cd kaemo ```

Installer les dépendances

``` composer install ```

Copier / Coller le fichier .env.example et le renommer en .env 

``` cp .env.example .env ```


Générer la clé d'application

``` php artisan key:generate ```

Modifier le .env pour y entrer la configuration de la base de données.

``` 
DB_HOST=localhost
DB_DATABASE=test_kaemo_vincent
DB_PORT=3306
DB_USERNAME=homestead
DB_PASSWORD=secret
``` 

Créer une nouvelle base de donnée mysql (je l'ai appelée test_kaemo_vincent)

Ensuite, lancer les migrations et installer les données de bases.

``` php artisan migrate ```

``` php artisan db:seed ```


La commande à éxécuter pour ajouter une nouvelle vidéo en base est:

``` php artisan video:register ```

### Routes:

/api/v1/videos

/api/v1/videos/:id

## Tests

A la racine du projet, lancer ``` phpunit ```


